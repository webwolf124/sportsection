<?php
/**
 * SportSection functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package SportSection
 */

if ( ! function_exists( 'sportsection_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function sportsection_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on SportSection, use a find and replace
		 * to change 'sportsection' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'sportsection', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'sportsection' ),
			'footer' => esc_html__( 'Footer', 'sportsection' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'sportsection_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'sportsection_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function sportsection_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'sportsection_content_width', 640 );
}
add_action( 'after_setup_theme', 'sportsection_content_width', 0 );

add_filter( 'get_search_form', 'my_search_form' );
function my_search_form( $form ) {

	$form = '
	<form role="search" class="search-form" method="get" id="searchform" action="' . home_url( '/' ) . '" >
		<input class="search-field" type="search" value="" name="s" id="s" placeholder="Найти..." />
		<input class="search-submit" type="submit" id="searchsubmit" value="" />
	</form>';

	return $form;
}

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function sportsection_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar базовый шаблон', 'sportsection' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'sportsection' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar шаблон "Секции/Экипировка"', 'sportsection' ),
		'id'            => 'cat-sidebar',
		'description'   => esc_html__( 'Add widgets here.', 'sportsection' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'sportsection_widgets_init' );


// Enable shortcodes in text widgets
add_filter('widget_text','do_shortcode');

/**
 * Enqueue scripts and styles.
 */
function sportsection_scripts() {
	wp_enqueue_style( 'sportsection-style', get_stylesheet_uri() );

	wp_enqueue_style( 'main-style', get_template_directory_uri() . '/css/main.css' );

	wp_enqueue_script( 'sportsection-common', get_template_directory_uri() . '/js/common.js', array(), '20151215', true );

	wp_enqueue_script( 'sportsection-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'sportsection-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	wp_enqueue_script( 'sportsection-mmenu', get_template_directory_uri() . '/libs/mmenu/jquery.mmenu.all.js', array(), '20151215', true );

	wp_enqueue_script( 'sportsection-owl-carousel', get_template_directory_uri() . '/libs/owl-carousel/owl.carousel.min.js', array(), '20151215', true );

	wp_enqueue_script( 'sportsection-equalheight', get_template_directory_uri() . '/libs/equalHeights/jquery.equalheights.min.js', array(), '20151215', true );

	wp_enqueue_script( 'sportsection-wow', get_template_directory_uri() . '/libs/wow/wow.min.js', array(), '20151215', true );

	wp_enqueue_script( 'sportsection-fancybox', get_template_directory_uri() . '/libs/fancybox/jquery.fancybox.min.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'sportsection_scripts' );

/**
 * Форматирование текста
 */
 remove_filter( 'the_content', 'wpautop' ); // Отключаем автоформатирование в полном тексте
 remove_filter( 'the_excerpt', 'wpautop' ); // Отключаем автоформатирование в анонсе

/**
 * Ограничение символов в функции the_excerpt()
 */
 function new_excerpt_length($length) {
   return 10;
 }
 add_filter('excerpt_length', 'new_excerpt_length');

 add_filter('excerpt_more', function($more) {
	return '';
});

/*    Редактируем профиль пользователя
========================================*/

//Изменение контактов
add_filter('user_contactmethods', 'my_user_contactmethods');

function my_user_contactmethods($user_contactmethods)
{

    $user_contactmethods['vkontakte'] = '<b>ВКонтакте</b>';

    return $user_contactmethods;
}

// запрет обновления выборочных плагинов
function filter_plugin_updates( $update ) {
    global $DISABLE_UPDATE; // см. wp-config.php
    if( !is_array($DISABLE_UPDATE) || count($DISABLE_UPDATE) == 0 ){  return $update;  }
    foreach( $update->response as $name => $val ){
        foreach( $DISABLE_UPDATE as $plugin ){
            if( stripos($name,$plugin) !== false ){
                unset( $update->response[ $name ] );
            }
        }
    }
    return $update;
}
add_filter( 'site_transient_update_plugins', 'filter_plugin_updates' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Customizer-custom additions.
 */
require get_template_directory() . '/inc/customizer-custom.php';

/**
 * Customizer-shortcode.
 */
require get_template_directory() . '/inc/custom-shortcode.php';

/**
 * Customizer-woocommerce.
 */
require get_template_directory() . '/inc/custom-woocommerce.php';

/**
 * Customizer-woocommerce.
 */
require get_template_directory() . '/widgets/vendor-widget.php';

// register Foo_Widget widget
function filter_vendor_widget() {
    register_widget( 'Filter_Vendor_Widget' );
}
add_action( 'widgets_init', 'filter_vendor_widget' );

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

@ini_set('display_errors', 0);

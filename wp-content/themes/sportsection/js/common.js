jQuery(document).ready(function($) {

  $('#site-mobile-menu').mmenu({
    extensions: [ 'multiline', 'position-right' ],
    navbar: {
      title: '<div class="mmenu-logo"></div>'
    },
    hooks: {
      'open:after': function() {
         $('.hamburger').addClass('is-active');
      },
      'close:before': function() {
         $('.hamburger').removeClass('is-active');
      },
    }
  });

  // Mobile shidden card category
  var productItem = document.getElementsByClassName('product-category product');

  function productMobileShow(productItem) {
    var countHidden = 0;

    for ( var i = 0; i < productItem.length; i++ ) {
      if ( i > 5 ) {
        countHidden++;
        productItem[i].style.display = 'none';
      }
    }
    $('.more-count span').text(countHidden);
  }

  function productDesctopShow(productItem) {
    for ( var i = 0; i < productItem.length; i++ ) {
      productItem[i].style.display = 'block';
    }
  }

  if ( screen.width <= 768 ) {
    productMobileShow(productItem);
  } else {
    productDesctopShow(productItem);
  }

  $('.more-count').click(function() {
    productDesctopShow(productItem);
    $(this).css('display', 'none');
  })

  $('<h2 class="section-title-category">Секции</h2>').insertAfter($('.term-description'));

  var sliderProduct = $('#shop-slider');
  var sliderProductThumbnails = $('#shop-slider-thumbnails');
  var slidesPerPage = 3;
  var syncedSecondary = true;

  sliderProduct.owlCarousel({
    items: 1,
    loop: false,
    dots: false,
  }).on('changed.owl.carousel', syncPosition);

  sliderProductThumbnails
  	.on('initialized.owl.carousel', function() {
  			sliderProductThumbnails.find(".owl-item").eq(0).addClass("current");
  	})
  	.owlCarousel({
  			nav: false,
  			margin: 10,
        loop: false,
  			smartSpeed: 200,
  			slideSpeed: 500,
  			dots: false,
  			slideBy: slidesPerPage, //alternatively you can slide by 1, this way the active slide will stick to the first item in the second carousel
  			responsiveRefreshRate: 100,
  			responsive: {
  				0: {
  					items: 2
  				},
  				576: {
  					item: slidesPerPage
  				}
  			}
  	}).on('changed.owl.carousel', syncPosition2);

    function syncPosition(el) {
				//if you set loop to false, you have to restore this next line
				var current = el.item.index;

				//if you disable loop you have to comment this block
				// var count = el.item.count - 1;
				// var current = Math.round(el.item.index - (el.item.count / 2) - .5);
        //
				// if (current < 0) {
				// 		current = count;
				// }
				// if (current > count) {
				// 		current = 0;
				// }

				//end block

				sliderProductThumbnails
						.find(".owl-item")
						.removeClass("current")
						.eq(current)
						.addClass("current");
				var onscreen = sliderProductThumbnails.find('.owl-item.active').length - 1;
				var start = sliderProductThumbnails.find('.owl-item.active').first().index();
				var end = sliderProductThumbnails.find('.owl-item.active').last().index();

				if (current > end) {
						sliderProductThumbnails.data('owl.carousel').to(current, 100, true);
				}
				if (current < start) {
						sliderProductThumbnails.data('owl.carousel').to(current - onscreen, 100, true);
				}
			}

      function syncPosition2(el) {
        if (syncedSecondary) {
            var number = el.item.index;
            sliderProduct.data('owl.carousel').to(number, 100, true);
        }
      }

      sliderProductThumbnails.on("click", ".owl-item", function(e) {
        e.preventDefault();
        var number = $(this).index();
        sliderProduct.data('owl.carousel').to(number, 300, true);
      });

      $('.wcmp_sorted_vendors').equalHeights();
      setTimeout(function() {
        $('.product-loop').equalHeights();
      }, 500);
      $('.useful-articles__info').equalHeights();




      $(' .filter-home .woof_container_select, .filter-home .woof_container_radio').addClass('col-lg-2 col-md-4');
      $('.filter-home .woof_container_slider').addClass('col-lg-4 col-md-5');
      $('.filter-home .woof_redraw_zone').addClass('row');
      $('.woof_submit_search_form').addClass('site-button');

      $('.page-id-15 .field_81 input').prop('checked', true);

      $('a[data-toggle=modal]').on('click', function(e) {
        e.preventDefault();
        var id = $(this).attr('href');
        $(id).fadeToggle('700', 'linear');
        $(id).css('display', 'flex');
        $('body').addClass('no-scroll');
      });

      $('.feedback__close').on('click', function() {
        $('.feedback').fadeOut('700', 'linear');
        $('body').removeClass('no-scroll');
      });

      if ($('input[data-slug=dlya-vzroslyh]').prop('checked') == true) {
        $('.woof_block_html_items').addClass('disabled');
      }

      $('input[data-slug=dlya-vzroslyh]').on('click', function() {
        $('.woof_block_html_items').addClass('disabled');
      });

      $('.home .woof').on('change',function() {
          if ( $('.home .woof select').val() == 0 &&
               $('.home .woof input:radio').prop('checked') == false &&
               $('.home .woof .irs-single').html() == undefined ) {
            $('.woof_submit_search_form_container button').css('opacity', '0');
          } else {
            $('.woof_submit_search_form_container button').css('opacity', '1');
          }
      });

    $('.woof').on('change',function() {

        if ($('input[data-slug=dlya-vzroslyh]').prop('checked') != true) {
          $('.woof_block_html_items').removeClass('disabled');
        } else {
          $('.woof_block_html_items').addClass('disabled');
        }


    });


    var navbar = $('.fixed-top');
    var navbarHeight = navbar.height();
    var newClass = 'scroll-top';
    var containerWidth = $('.container').outerWidth();

    $(window).resize(function(event) {
      navbarHeight = navbar.height();
      containerWidth = $('.container').outerWidth();
    });

    $(window).scroll(function() {
      if( $(this).scrollTop() > 0 ) {
        navbar.addClass(newClass);
        navbar.css('width', containerWidth);
      } else {
        navbar.removeClass(newClass)
      }
    });

});

<?php
/**
 * Single Product Thumbnails
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-thumbnails.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.3.2
 */

defined( 'ABSPATH' ) || exit;

// Note: `wc_get_gallery_image_html` was added in WC 3.3.2 and did not exist prior. This check protects against theme overrides being used on older versions of WC.
if ( ! function_exists( 'wc_get_gallery_image_html' ) ) {
	return;
}

global $post, $product, $woocommerce;

$attachment_ids = $product->get_gallery_image_ids();
$post_thumbnail_id = $product->get_image_id();

if ( $post_thumbnail_id ) {
    $loop       = 0;
    $columns    = apply_filters( 'woocommerce_product_thumbnails_columns', 3 );
    ?>
    <div id="shop-slider" class="owl-carousel product-image">
			<?php
						$image_link = wp_get_attachment_url( $post_thumbnail_id );
						$image_title = esc_attr( get_the_title( $post_thumbnail_id ) );
						$image_caption = esc_attr( get_post_field( 'post_excerpt', $post_thumbnail_id ) );
						$image = wp_get_attachment_image( $post_thumbnail_id, apply_filters( 'single_product_small_thumbnail_size', 'shop_single' ), 0, $attr = array(
								'title' => $image_title,
								'alt'   => $image_title
								) );
						?>
						<a data-fancybox="images" href="<?php echo $image_link; ?>">
							<span class="zoom"></span>
						<?php
						echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', sprintf( '%s', $image ), $post_thumbnail_id, $post->ID);
						?>
						</a>
						<?php
						if ( $attachment_ids ) {
            	foreach ( $attachment_ids as $attachment_id ) {
                $image_link = wp_get_attachment_url( $attachment_id );
                if ( ! $image_link )
                    continue;
                $image_title = esc_attr( get_the_title( $attachment_id ) );
                $image_caption = esc_attr( get_post_field( 'post_excerpt', $attachment_id ) );

                $image = wp_get_attachment_image( $attachment_id, apply_filters( 'single_product_small_thumbnail_size', 'shop_single' ), 0, $attr = array(
                    'title' => $image_title,
                    'alt'   => $image_title
                    ) );
										?>
										<a data-fancybox="images" href="<?php echo $image_link; ?>">
											<span class="zoom"></span>
                			<?php echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', sprintf( '%s', $image ), $attachment_id, $post->ID ); ?>
										</a>
									<?php
                $loop++;
            }
						}
        ?>
    </div>
		<div id="shop-slider-thumbnails" class="owl-carousel product-image-thumbnails">
			<?php
						$image_link = wp_get_attachment_url( $post_thumbnail_id );
						$image_title = esc_attr( get_the_title( $post_thumbnail_id ) );
						$image_caption = esc_attr( get_post_field( 'post_excerpt', $post_thumbnail_id ) );
						$image = wp_get_attachment_image( $post_thumbnail_id, apply_filters( 'single_product_small_thumbnail_size', 'shop_single' ), 0, $attr = array(
								'title' => $image_title,
								'alt'   => $image_title
								) );
						echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', sprintf( '%s', $image ), $post_thumbnail_id, $post->ID);

						if ( $attachment_ids ) {
							foreach ( $attachment_ids as $attachment_id ) {
									$image_link = wp_get_attachment_url( $attachment_id );
									if ( ! $image_link )
											continue;
									$image_title = esc_attr( get_the_title( $attachment_id ) );
									$image_caption = esc_attr( get_post_field( 'post_excerpt', $attachment_id ) );

									$image = wp_get_attachment_image( $attachment_id, apply_filters( 'single_product_small_thumbnail_size', 'shop_single' ), 0, $attr = array(
											'title' => $image_title,
											'alt'   => $image_title
											) );

									echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', sprintf( '%s', $image ), $attachment_id, $post->ID );

									$loop++;
							}
						}
				?>
		</div>
    <?php
}

<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package SportSection
 */

?>

	<div class="col-lg-4 col-sm-6 wow flipInY" data-wow-duration="2s" data-wow-delay="0.1s">
		<a class="useful-articles__link" href="<?php permalink_link(); ?>">
			<div class="useful-articles__block">
				<?php the_post_thumbnail(); ?>
				<div class="useful-articles__info">
					<h3 class="useful-articles__title"><?php the_title(); ?></h3>
					<div class="useful-articles__content">
						<p class="useful-articles__date"><?php the_time('d.m.Y'); ?></p>
							<p class="useful-articles__content"><?php the_excerpt(); ?></p>
					</div>
				</div>
			</div>
		</a>
	</div>

	<div class="entry-content">
		<?php

		wp_link_pages( array(
			'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'sportsection' ),
			'after'  => '</div>',
		) );
		?>
	</div><!-- .entry-content -->

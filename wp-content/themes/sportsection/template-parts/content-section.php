<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package SportSection
 */

?>


<div class="container">
	<div class="row">
		<div class="col-md-12 bg-broadcrumbs">
			<?php
				if ( function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb('<div id="breadcrumbs" class="breadcrumbs">','</div>');
				}
			?>
		</div>
	</div>
</div>
<div class="entry-content">
	<div class="container">
		<div class="row">
			<div class="col-md-3 bg-main-content order-md-1 order-2">
				<?php if ( is_active_sidebar( 'cat-sidebar' ) ) : ?>

					<?php dynamic_sidebar( 'cat-sidebar' ); ?>

			<?php endif; ?>
			</div>
			<div class="col-md-9 bg-main-content order-md-2 order-1">
				<?php the_content(); 	?>
			</div>
		</div>
	</div>
</div><!-- .entry-content -->

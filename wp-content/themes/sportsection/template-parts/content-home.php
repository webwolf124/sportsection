<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package SportSection
 */

?>


<div class="entry-content">
	<div class="container">
		<div class="row">
			<div class="col-md-12 bg-main-content">
				<?php the_content(); 	?>
			</div>
		</div>
	</div>
</div><!-- .entry-content -->

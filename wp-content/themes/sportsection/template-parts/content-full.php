<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package SportSection
 */

?>

<div class="container">
	<div class="row">
		<div class="col-md-12 bg-broadcrumbs">
			<?php
				if ( function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb('<div id="breadcrumbs" class="breadcrumbs">','</div>');
				}
			?>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-md-12 bg-main-content order-md-2 order-1">
			<header class="entry-header">
				<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
			</header><!-- .entry-header -->

			<div class="entry-content">
				<?php
				the_content();
				?>
			</div><!-- .entry-content -->
		</div>
	</div>
</div>

<?php
add_action('customize_register', function($customizer) {
    // Настройки сайта
     $customizer->add_section(
             'settings-site', array(
         'title' => 'Настройки сайта',
         'description' => 'Контактная информация на сайте',
         'priority' => 11,
             )
     );
    $customizer->add_setting('footer-image');
    $customizer->add_control(
      new WP_Customize_Upload_Control(
        $customizer, 'footer-image', array(
          'label' => 'Изображение в футоре',
          'section' => 'settings-site',
          'settings' => 'footer-image'
        )
      )
    );
    $customizer->add_setting(
      'insta', array('default' => '')
    );
    $customizer->add_control(
      'insta',
      array(
        'label' => 'Instagram',
        'section' => 'settings-site',
        'type' => 'text',
        'description' => 'Добавте ссылку(при отсутсвии сылки оконка пропадает)'
      )
    );
    $customizer->add_setting(
      'vk', array('default' => '')
    );
    $customizer->add_control(
      'vk',
      array(
        'label' => 'Вконтакте',
        'section' => 'settings-site',
        'type' => 'text',
        'description' => 'Добавте ссылку(при отсутсвии сылки оконка пропадает)'
      )
    );
    $customizer->add_setting(
      'facebook', array('default' => '')
    );
    $customizer->add_control(
      'facebook',
      array(
        'label' => 'Facebook',
        'section' => 'settings-site',
        'type' => 'text',
        'description' => 'Добавте ссылку(при отсутсвии сылки оконка пропадает)'
      )
    );
    $customizer->add_setting(
      'ok', array('default' => '')
    );
    $customizer->add_control(
      'ok',
      array(
        'label' => 'Одноклассники',
        'section' => 'settings-site',
        'type' => 'text',
        'description' => 'Добавте ссылку(при отсутсвии сылки оконка пропадает)'
      )
    );
    $customizer->add_setting(
      'ask-question', array('default' => '')
    );
    $customizer->add_control(
      'ask-question',
      array(
        'label' => 'Форма для кнопки "Задать вопрос"',
        'section' => 'settings-site',
        'type' => 'text',
        'description' => 'Впишите id contact-from 7.'
      )
    );
    $customizer->add_setting(
      'ask-admin', array('default' => '')
    );
    $customizer->add_control(
      'ask-admin',
      array(
        'label' => 'Форма для кнопки "Задать вопрос администрации"',
        'section' => 'settings-site',
        'type' => 'text',
        'description' => 'Впишите id contact-from 7.'
      )
    );

  });

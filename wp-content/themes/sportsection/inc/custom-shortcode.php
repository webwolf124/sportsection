<?php

/**
 * Шорткод вывода новостей
 */
function usefulArticles( $atts ) {

	$params = shortcode_atts( [
		'showposts' => 5,
		'category_name' => 'poleznye-stati',
		'description' => 1,
		'grid' => 1
	], $atts );
  $the_query = new WP_Query( $params );

  $posts = '';

  while($the_query->have_posts()) : $the_query->the_post();
      if ( $params["grid"] ) {
				$posts .= '	<div class="col-lg-4 col-sm-6 wow flipInY" data-wow-duration="2s" data-wow-delay="0.1s">';
			}
			$posts .= '
				<a class="useful-articles__link" href="' . get_the_permalink() . '">
				<div class="useful-articles__block">
	        ' . get_the_post_thumbnail() . '
					<div class="useful-articles__info">
	        <h3 class="useful-articles__title">' . get_the_title() . '</h3>
					<div class="useful-articles__content">
		        <p class="useful-articles__date">' . get_the_time('d.m.Y') . '</p>';
						if ( $params["description"] ) {
							$posts .= '<p class="useful-articles__content">' . get_the_excerpt() . '</p>';
						}
        $posts .= '
					</div>
				</div>
      </div></a>';
			if ( $params["grid"] ) {
				$posts .= '	</div>';
			}
  endwhile;

  wp_reset_query();

  return $posts;
}
add_shortcode( 'useful-articles', 'usefulArticles' );

<?php

remove_action( 'woocommerce_before_shop_loop' , 'woocommerce_result_count', 20, 0 );

remove_action( 'woocommerce_before_shop_loop' , 'woocommerce_catalog_ordering', 30, 0 );

remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0 );

remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10, 0);

add_action( 'woocommerce_shop_loop_item_title', 'woocommerce_template_loop_attributes', 20 );

add_action( 'woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_link_button', 20 );

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5, 0 );

add_action( 'woocommerce_before_single_product_summary', 'woocommerce_template_single_title', 5 );

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40, 0 );

add_action( 'woocommerce_single_product_summary', 'woocommerce_output_product_data_tabs', 40);

add_action( 'woocommerce_single_product_summary', 'localSection', 50 );

remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs' );

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10, 0 );

add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );

add_action( 'woocommerce_after_single_product_summary', 'woo_tab_description', 5 );

remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20, 0 );

function woo_tab_description() {

	global $post;

	$heading = esc_html( apply_filters( 'woocommerce_product_description_heading', __( 'Description', 'woocommerce' ) ) );

	if ( $heading ) :
		echo '<div class="single-product__description">';
	  echo '<h2>'. $heading .'</h2>';

	endif;

	the_content();
	echo '</div>';
}

function woo_remove_product_tabs( $tabs ) {

    unset( $tabs['description'] );      	// Удаляет таб "описание"
    unset( $tabs['reviews'] ); 			// Удаляет таб "обзоры/отзывы"

    return $tabs;
}

function woocommerce_template_loop_attributes () {
		global $product;

    $attribute_names_year = get_the_terms($product->get_id(), 'pa_vozrast-let');
    $attribute_names_sex = get_the_terms($product->get_id(), 'pa_gruppa');

    $min = $max = 0;

		echo '<div class="woocommerce-loop-product-attr">';
      echo '<div class="woocommerce-loop-product-attr-block">';
        foreach ($attribute_names_sex as $sex) {
          echo '<span class="woocommerce-loop-product-value">';
            echo $sex->name;
          echo '</span>';
        }
        foreach ($attribute_names_year as $year) {
          if ( $year->name <= $min ) {
            $min = $year->name;
          }
          if ( $year->name > $max ) {
            $max = $year->name;
          }
        }
        if ( $min == $max ) {
          echo '<span class="woocommerce-loop-product-value">c ' . $min . ' лет</span>';
        } else {
          echo '<span class="woocommerce-loop-product-value">' . $min . ' - ' . $max . ' лет</span>';
        }
      echo '</div>';
		echo '</div>';
}

function woocommerce_template_loop_product_link_button () {
	global $product;

	$link = apply_filters( 'woocommerce_loop_product_link', get_the_permalink(), $product );

	echo '<a class="site-button" href="' . esc_url( $link ) . '">Подробнее</a>';

}

function localSection () {
	$field_location = "address";
	$field_phone = "phone";
	$fieldLocation = get_field_object($field_location);
	$fieldPhone = get_field_object($field_phone);

	if($fieldPhone['value'] || $fieldLocation['value']) {
		$location = '
			<div class="single-product__location">
				<div class="single-product__location-block__label">
					'. $fieldLocation['label'] .'
				</div>
				<div class="single-product__location-block__value">
					'. $fieldLocation['value'] .'
				</div>
				<div class="single-product__location-block__label">
					'. $fieldPhone['label'] .'
				</div>
				<div class="single-product__location-block__value">
					<a href="tel:'. $fieldPhone['value'] .'">'. $fieldPhone['value'] .'</a>
				</div>
			</div>
		';
	}

	echo $location;
}

function mytheme_add_woocommerce_support() {
	add_theme_support( 'woocommerce' );
}
add_action( 'after_setup_theme', 'mytheme_add_woocommerce_support' );

add_theme_support( 'wc-product-gallery-lightbox' );
add_theme_support( 'wc-product-gallery-slider' );

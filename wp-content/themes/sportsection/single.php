<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package SportSection
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<div class="container">
				<div class="row">
					<div class="col-md-12 bg-broadcrumbs">
						<?php
							if ( function_exists('yoast_breadcrumb') ) {
								yoast_breadcrumb('<div id="breadcrumbs" class="breadcrumbs">','</div>');
							}
						?>
					</div>
				</div>
			</div>
			<div class="container">
				<div class="row">
					<div class="col-lg-3 bg-main-content order-lg-1 order-2">
							<?php get_sidebar() ?>
						</div>
						<div class="col-lg-9 bg-main-content order-lg-2 order-1">
							<?php
							while ( have_posts() ) :
								the_post();

								get_template_part( 'template-parts/content' );

							endwhile; // End of the loop.
							?>
					</div>
				</div>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();

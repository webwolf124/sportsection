<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package SportSection
 */

?>
	<footer id="colophon" class="site-footer">
		<div class="container">
			<div class="row site-footer__info">
				<div class="col-lg-4 order-2 order-lg-2">
					<nav id="footer-menu" class="footer-menu">
						<?php
						wp_nav_menu( array(
							'theme_location' => 'footer',
							'menu_id'        => 'footer-menu-child',
						) );
						?>
					</nav>
				</div>
				<div class="col-lg-5 order-1 order-lg-2">
					<div class="site-footer__logo">
						<?php if(!get_theme_mod('footer-image') == '') : ?>
							<img src="<?php echo get_theme_mod('footer-image', '#'); ?>"></a>
						<?php endif ?>
						<?php
						$sportsection_description = get_bloginfo( 'description', 'display' );
						if ( $sportsection_description || is_customize_preview() ) :
							?>
							<p class="site-description"><?php echo $sportsection_description; /* WPCS: xss ok. */ ?></p>
						<?php endif; ?>
					</div>
				</div>
				<div class="col-lg-3 order-3 order-lg-3">
					<a href="#<?php echo get_theme_mod('ask-admin', ''); ?>" data-toggle="modal" class="site-button">Написать администрации</a>
					<div class="site-footer__social">
						<p class="site-footer__social__text">Мы в соцсетях:</p>
						<?php if(!get_theme_mod('vk') == '') : ?>
							<a class="vk-icon" target="_blank" href="<?php echo get_theme_mod('vk', '#'); ?>"></a>
						<?php endif ?>
						<?php if(!get_theme_mod('facebook') == '') : ?>
							<a class="facebook-icon" target="_blank" href="<?php echo get_theme_mod('facebook', '#'); ?>"></a>
						<?php endif ?>
						<?php if(!get_theme_mod('ok') == '') : ?>
							<a class="ok-icon" target="_blank" href="<?php echo get_theme_mod('ok', '#'); ?>"></a>
						<?php endif ?>
						<?php if(!get_theme_mod('insta') == '') : ?>
							<a class="insta-icon" target="_blank" href="<?php echo get_theme_mod('insta', '#'); ?>"></a>
						<?php endif ?>
					</div>
				</div>
				</div>
			<div class="row">
				<div class="col-md-12 site-footer__develop">
					<p class="site-footer__privat">© 2018. Все права защищены</p>
					<a class="site-footer__develop__company" target="_blank" href="https://www.federacel.ru/" rel="nofollow">Разработка —  ИА «Цель»</a>
				</div>
			</div>
		</div>
	</footer><!-- #colophon -->
<?php wp_footer(); ?>
<script>
	new WOW().init();
</script>
</body>
</html>

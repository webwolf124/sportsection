<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package SportSection
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<div class="container">
				<div class="row">
					<div class="col-md-12 bg-broadcrumbs">
						<?php
							if ( function_exists('yoast_breadcrumb') ) {
								yoast_breadcrumb('<div id="breadcrumbs" class="breadcrumbs">','</div>');
							}
						?>
					</div>
				</div>
			</div>
			<div class="entry-content">
				<div class="container">
					<div class="row">
						<div class="col-md-3 bg-main-content order-md-1 order-2">
							<?php if ( is_active_sidebar( 'cat-sidebar' ) ) : ?>

								<?php dynamic_sidebar( 'cat-sidebar' ); ?>

						<?php endif; ?>
						</div>
						<div class="col-md-9 bg-main-content order-md-2 order-1">
							<section class="error-404 not-found">
								<header class="page-header">
									<h1 class="page-title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'sportsection' ); ?></h1>
								</header><!-- .page-header -->

								<div class="page-content">
									<p><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'sportsection' ); ?></p>

									<?php
									get_search_form();

									the_widget( 'WP_Widget_Recent_Posts' );
									?>

									<div class="widget widget_categories">
										<h2 class="widget-title"><?php esc_html_e( 'Most Used Categories', 'sportsection' ); ?></h2>
										<ul>
											<?php
											wp_list_categories( array(
												'orderby'    => 'count',
												'order'      => 'DESC',
												'show_count' => 1,
												'title_li'   => '',
												'number'     => 10,
											) );
											?>
										</ul>
									</div><!-- .widget -->

									<?php
									/* translators: %1$s: smiley */
									$sportsection_archive_content = '<p>' . sprintf( esc_html__( 'Try looking in the monthly archives. %1$s', 'sportsection' ), convert_smilies( ':)' ) ) . '</p>';
									the_widget( 'WP_Widget_Archives', 'dropdown=1', "after_title=</h2>$sportsection_archive_content" );

									the_widget( 'WP_Widget_Tag_Cloud' );
									?>

								</div><!-- .page-content -->
							</section><!-- .error-404 -->
						</div>
					</div>
				</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();

<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package SportSection
 */

?>
<!doctype html>
<html <?php language_attributes(); ?> style="	margin: 0 !important;">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'sportsection' ); ?></a>
	<!-- Preload -->
	<div id="hellopreloader"><div id="hellopreloader_preload"></div></div>
	<script>
		var hellopreloader = document.getElementById("hellopreloader_preload");
		function fadeOutnojquery(el){
			el.style.opacity = 1;
			var interhellopreloader = setInterval(function(){
				el.style.opacity = el.style.opacity - 0.05;
				if (el.style.opacity <=0.05){
					clearInterval(interhellopreloader);
					hellopreloader.style.display = "none";
				}},16);
		}
		window.onload = function(){
			setTimeout(function(){
				fadeOutnojquery(hellopreloader);
		},1000);};
	</script>
	<!-- ./Preload -->
	<header id="masthead" class="site-header">
		<div class="site-branding">
			<div class="container">
				<div class="row row-header-menu fixed-top">
					<div class="col-md-12">
							<div class="row align-items-center">
								<div class="col-md-9">
									<a href="#site-mobile-menu" class="hamburger hamburger--arrowalt-r">
										<span class="hamburger-box">
											<span class="hamburger-inner"></span>
										</span>
									</a>
									<nav id="site-mobile-menu" class="main-navigation-mobile">

										<?php
										wp_nav_menu( array(
											'theme_location' => 'menu-1',
											'menu_id'        => 'primary-menu',
										) );
										?>
									</nav>
									<nav id="site-navigation" class="main-navigation">
										<?php
										wp_nav_menu( array(
											'theme_location' => 'menu-1',
											'menu_id'        => 'primary-menu',
										) );
										?>
									</nav>
								</div>
								<div class="col-md-2 line-left offset-md-1">
									<div class="site-header__social">
										<?php if(!get_theme_mod('vk') == '') : ?>
											<a class="vk-icon" target="_blank" href="<?php echo get_theme_mod('vk', '#'); ?>"></a>
										<?php endif ?>
										<?php if(!get_theme_mod('facebook') == '') : ?>
											<a class="facebook-icon" target="_blank" href="<?php echo get_theme_mod('facebook', '#'); ?>"></a>
										<?php endif ?>
										<?php if(!get_theme_mod('ok') == '') : ?>
											<a class="ok-icon" target="_blank" href="<?php echo get_theme_mod('ok', '#'); ?>"></a>
										<?php endif ?>
										<?php if(!get_theme_mod('insta') == '') : ?>
											<a class="insta-icon" target="_blank" href="<?php echo get_theme_mod('insta', '#'); ?>"></a>
										<?php endif ?>
									</div>
								</div>
						</div>
					</div>
				</div>
				<div class="row align-items-center row-header">
					<div class="col-md-5">
						<div class="site-header__logo">
							<?php
							the_custom_logo();
							?>
							<span class="site-header__line"></span>
							<?php
							$sportsection_description = get_bloginfo( 'description', 'display' );
							if ( $sportsection_description || is_customize_preview() ) :
								?>
								<p class="site-description"><?php echo $sportsection_description; /* WPCS: xss ok. */ ?></p>
							<?php endif; ?>
						</div>
					</div>
					<div class="col-md-4">
						<?php get_search_form(); ?>
					</div>
					<div class="col-lg-2 offset-lg-1 col-md-3">
						<a href="#<?php echo get_theme_mod('ask-question', ''); ?>" data-toggle="modal" class="site-button" >Задать вопрос</a>
					</div>
				</div>
			</div>
		</div><!-- .site-branding -->
	</header><!-- #masthead -->

	<div id="<?php echo get_theme_mod('ask-question', ''); ?>" class="feedback">
		<div class="feedback__form">
			<div class="feedback__close"></div>
			<?php echo do_shortcode('[contact-form-7 id="'. get_theme_mod('ask-question', '') .'"]'); ?>
		</div>
	</div>
	<div id="<?php echo get_theme_mod('ask-admin', ''); ?>" class="feedback">
		<div class="feedback__form">
			<div class="feedback__close"></div>
			<?php echo do_shortcode('[contact-form-7 id="'. get_theme_mod('ask-admin', '') .'"]'); ?>
		</div>
	</div>

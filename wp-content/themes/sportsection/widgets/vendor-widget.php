<?php
/**
 * Adds Foo_Widget widget.
 */
class Filter_Vendor_Widget extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'filter_vendor_widget', // Base ID
			esc_html__( 'Filter Vendor', 'text_domain' ), // Name
			array( 'description' => esc_html__( 'Custom filter vendor', 'text_domain' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		echo $args['before_widget'];
		?>
    <form name="vendor_sort" method="get">
        <div class="vendor_sort">
            <?php
            $product_category = get_terms('product_cat');
						$product_tag = get_terms('product_tag');
            $options_html = '';
						$options_html .= '<option selected disabled>Категории</option>';
						$selected_category = $_REQUEST['vendor_sort_category'];
            foreach ($product_category as $category) {
                if ($category->term_id == $selected_category) {
                    $options_html .= '<option selected value="' . esc_attr($category->term_id) . '">' . esc_html($category->name) . '</option>';
                } else {
                    $options_html .= '<option value="' . esc_attr($category->term_id) . '">' . esc_html($category->name) . '</option>';
                }
            }
            ?>
						<div class="filter-block">
							<label class="vandor-select__title">Вид спорта</label>
							<select name="vendor_sort_category" id="vendor_sort_category" class="select"><?php echo $options_html; ?></select>
						</div>
						<div class="filter-block custom-filter-radio">
							<label class="vandor-select__title">Для чего</label>
							<?php foreach ($product_tag as $tag) { ?>
								<div class="filter-block-tag field_<?php echo esc_attr($tag->term_id) ?>">
								 	<input id="tag-name-<?php echo esc_attr($tag->term_id) ?>" type="radio" name="product-tag" value="<?php echo esc_attr($tag->term_id) ?>"
									<?php
										if ($_REQUEST['product-tag'] == $tag->term_id) {
											echo "checked";
										}
									?>
									/>
									<label for="tag-name-<?php echo esc_attr($tag->term_id) ?>"><?php echo esc_attr($tag->name) ?></label>
								</div>
							<?php } ?>
						</div>
<?php do_action('wcmp_vendor_list_vendor_sort_extra_attributes'); ?>
            <input value="<?php echo __('Sort', 'dc-woocommerce-multi-vendor'); ?>" class="site-button" type="submit">
        </div>
    </form>
    <?php echo $args['after_widget'];
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		$title = ! empty( $instance['title'] ) ? $instance['title'] : esc_html__( 'New title', 'text_domain' );
		?>
		<?php
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? sanitize_text_field( $new_instance['title'] ) : '';

		return $instance;
	}

} // class Foo_Widget

<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

 if ( file_exists( dirname( __FILE__ ) . '/wp-config-local.php' ) ) {
 	include( dirname( __FILE__ ) . '/wp-config-local.php' );

	define('WP_DEBUG', true);
 }
 else {
 	// Настройки базы данных на веб-сервере
 	define('DB_NAME', 'mas_sporta');
 	define('DB_USER', 'mas-sporta');
 	define('DB_PASSWORD', 'Hjuh&^%&*hhhuj(&^%#@!dDD');
 	define('DB_HOST', 'localhost');

	/**
	 * Для разработчиков: Режим отладки WordPress.
	 *
	 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
	 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
	 * в своём рабочем окружении.
	 *
	 * Информацию о других отладочных константах можно найти в Кодексе.
	 *
	 * @link https://codex.wordpress.org/Debugging_in_WordPress
	 */
	define('WP_DEBUG', false);
  define('DISALLOW_FILE_EDIT', true);
  define('WPCF7_AUTOP', false );
 }

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'E`CQ,wOq<3|Pips>i~k:UrWY6RShn(4=OzTa8o;IvWWqt ^+ZvRtBFB<aHDP(Ow~');
define('SECURE_AUTH_KEY',  'j:t{vB;&N97mT@TI-1>H=X!8kM,D6;uxobI40hP}*^&!_8/( >]nsU&@6X(3b}/H');
define('LOGGED_IN_KEY',    '*Fs&dL >A8? `92nv8D&fIe[C[;W5sKmmZX[|[sz~.+MbL<N_i qXAxfnffM5TC_');
define('NONCE_KEY',        'R,%-xxf[opbrpp~_}x$/Xyh@^Wl:]mBrko6g{df59S<WpxzpM54_={Zz,qRzhmPs');
define('AUTH_SALT',        '@&i/<_*r{0 [GzLK.(/jWtR+4y|F#=_|9En4m}7@__!kTs#i!2MP`-CF +WSXhRB');
define('SECURE_AUTH_SALT', 'D8k 6GX-HGH5I>~kDb^etagUT}5>J|C!Q9yP~X~+r5+|<)&OgAo!P#Q|$L}_54iD');
define('LOGGED_IN_SALT',   '5d@7@eHqIL*+PENJ(PgqBT+`!s3pzi?;k#8RY-o`WjS:43,@knb.`hn7L3E}YrN-');
define('NONCE_SALT',       'N&E6fPPk}#pU<)4Iy`[O8V9$<w,(*LTZ?/4B>))Sn1j?}wFU`/7~<7OMHw+*`X:(');

/**#@-*/

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8');
/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';
$DISABLE_UPDATE = array( 'dc-woocommerce-multi-vendor');

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
